﻿using FastshippingCar.Web.Common;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Services.Interfaces;
using Firebase.Auth;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Services
{
    public class RolServiceImpl : IRolService
    {
        private readonly IDaoGenerico<RolesModel> _rol;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RolServiceImpl(IHttpContextAccessor httpContextAccessor
            , IDaoGenerico<RolesModel> rol)
        {
            _rol = rol;
            _httpContextAccessor = httpContextAccessor;
        }

        public Task<RolesModel> RolUsuario()
        {
            try
            {
                return _rol.Tomar(Helpers.RolId(_httpContextAccessor.HttpContext.User));
            }
            catch (FirebaseAuthException e)
            {
                Console.WriteLine(e.Reason.ToString());
                throw;
            }
        }
    }
}
