﻿using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace FastshippingCar.Web.Services
{
    public static class ServiceConfiguration
    {
        public static void ServiceConfigurar(this IServiceCollection services)
        {
            services.AddScoped(typeof(IServiceGenerico<>), typeof(ServiceGenericoImpl<>));
            services.AddScoped<IUsuarioService, UsuarioServiceImpl>();
            services.AddScoped<IRolService, RolServiceImpl>();
            services.AddScoped<IReputacionVendedorService, ReputacionVendedorServiceImpl>();
            services.AddScoped<ICalificacionProductoService, CalificacionProductoServiceImpl>();
        }
    }
}
