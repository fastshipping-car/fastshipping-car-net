﻿using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Services.Interfaces;
using Firebase.Database.Query;
using System.Linq;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Services
{
    public class CalificacionProductoServiceImpl : ICalificacionProductoService
    {
        private readonly IDaoGenerico<ProductoPuntuacionModel> _productoPuntuacionDao;

        public CalificacionProductoServiceImpl(IDaoGenerico<ProductoPuntuacionModel> productoPuntuacionDao)
        {
            _productoPuntuacionDao = productoPuntuacionDao;
        }

        public async Task<decimal> ConsultarCalificacionProducto(string productoId)
        {
            var queryProductos = _productoPuntuacionDao.ChildQuery
                                                        .OrderBy(nameof(ProductoPuntuacionModel.ProductoId))
                                                        .EqualTo(productoId);

            var productos = (await _productoPuntuacionDao.Consultar(queryProductos));
            var personasQueVotaron5 = productos.Count(x => x.ValorPuntuacion == 5);
            var personasQueVotaron4 = productos.Count(x => x.ValorPuntuacion == 4);
            var personasQueVotaron3 = productos.Count(x => x.ValorPuntuacion == 3);
            var personasQueVotaron2 = productos.Count(x => x.ValorPuntuacion == 2);
            var personasQueVotaron1 = productos.Count(x => x.ValorPuntuacion == 1);

            if (productos.Any())
            {
                return ((personasQueVotaron5 * 5) + (personasQueVotaron4 * 4) + (personasQueVotaron3 * 3) + (personasQueVotaron2 * 2) + (personasQueVotaron1 * 1)) / productos.Count;
            }
            else
            {
                return 0;
            }
        }
    }
}
