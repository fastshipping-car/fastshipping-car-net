﻿using FastshippingCar.Web.Common;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Services.Interfaces;
using Firebase.Database.Query;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Services
{
    public class ReputacionVendedorServiceImpl : IReputacionVendedorService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IServiceGenerico<VendedorModel> _vendedor;
        private readonly IServiceGenerico<TiendaModel> _tienda;
        private readonly IDaoGenerico<HistorialCompraModel> _historialCompraDao;
        private readonly IDaoGenerico<ProductoPuntuacionModel> _productoPuntuacionDao;

        public ReputacionVendedorServiceImpl(IHttpContextAccessor httpContextAccessor
            , IServiceGenerico<VendedorModel> vendedor
            , IServiceGenerico<TiendaModel> tienda
            , IDaoGenerico<HistorialCompraModel> historialCompraDao
            , IDaoGenerico<ProductoPuntuacionModel> productoPuntuacionDao
            , IServiceGenerico<UsuarioModel> usuario)
        {
            _httpContextAccessor = httpContextAccessor;
            _vendedor = vendedor;
            _tienda = tienda;
            _historialCompraDao = historialCompraDao;
            _productoPuntuacionDao = productoPuntuacionDao;
        }

        public async Task<decimal> ConsultarReputacionVendedor(string tiendaId)
        {
            var tienda = await _tienda.Tomar(tiendaId);
            var queryCarritoCompra = _historialCompraDao.ChildQuery
                                                        .OrderBy(nameof(CarritoCompraModel.TiendaId))
                                                        .EqualTo(tiendaId);

            var historialCompra = (await _historialCompraDao.Consultar(queryCarritoCompra))
                                    .Where(x => x.EstadoCompra == Models.Enums.EstadoCompra.Finalizado);

            if (historialCompra.Count() < 10)
            {
                return 0;
            }

            var productoPuntuacion = (await _productoPuntuacionDao.Consultar())
                                            .Where(x => historialCompra.Select(y => y.Producto.Id).Contains(x.ProductoId));

            return (productoPuntuacion.Sum(x => x.ValorPuntuacion) / productoPuntuacion.Count()) / 100;
        }
    }
}
