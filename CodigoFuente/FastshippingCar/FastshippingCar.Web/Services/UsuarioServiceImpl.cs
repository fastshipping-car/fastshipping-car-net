﻿using FastshippingCar.Web.Dto;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Services.Interfaces;
using Firebase.Auth;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;

using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Services
{
    public class UsuarioServiceImpl : IUsuarioService
    {
        private readonly IUsuarioDao _usuarioDao;
        private readonly LinkGenerator _linkGenerator;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IServiceGenerico<UsuarioModel> _usuario;

        public UsuarioServiceImpl(IUsuarioDao usuarioDao
            , LinkGenerator linkGenerator
            , IHttpContextAccessor httpContextAccessor
            , IServiceGenerico<UsuarioModel> usuario)
        {
            _usuarioDao = usuarioDao;
            _linkGenerator = linkGenerator;
            _httpContextAccessor = httpContextAccessor;
            _usuario = usuario;
        }

        public async Task<FirebaseAuthLink> AutenticarUsuario(string email, string contrasena)
        {
            try
            {
                var firebaseAuthLink = await _usuarioDao.AutenticarUsuario(email, contrasena);
                var usuario = await _usuario.Tomar(firebaseAuthLink.User.LocalId);
                var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.NameIdentifier, string.IsNullOrEmpty(firebaseAuthLink.User.DisplayName) ? firebaseAuthLink.User.Email : firebaseAuthLink.User.DisplayName),
                        new Claim(ClaimTypes.Name, string.IsNullOrEmpty(firebaseAuthLink.User.FirstName) ? firebaseAuthLink.User.Email : firebaseAuthLink.User.FirstName),
                        new Claim(ClaimTypes.Email, firebaseAuthLink.User.Email),
                        new Claim(ClaimTypes.Role, usuario?.RolId ?? string.Empty),
                        new Claim(ClaimTypes.Sid, firebaseAuthLink.FirebaseToken),
                        new Claim(ClaimTypes.PrimarySid, firebaseAuthLink.User.LocalId),
                        new Claim(ClaimTypes.IsPersistent, !firebaseAuthLink.IsExpired() ? bool.TrueString : bool.FalseString),
                    };

                var authProperties = new AuthenticationProperties
                {
                    ExpiresUtc = DateTimeOffset.UtcNow.AddSeconds(firebaseAuthLink.ExpiresIn),
                    IsPersistent = !firebaseAuthLink.IsExpired(),
                    RedirectUri = _linkGenerator.GetPathByPage(_httpContextAccessor.HttpContext, "/Login")
                };

                _httpContextAccessor.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.NameIdentifier, ClaimTypes.Role));

                await _httpContextAccessor.HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    _httpContextAccessor.HttpContext.User,
                    authProperties);


                usuario.Token = firebaseAuthLink.User.LocalId;
                return firebaseAuthLink;
            }
            catch (AuthenticationException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            catch (FirebaseAuthException e)
            {
                Console.WriteLine(e.Reason.ToString());
                throw;
            }
        }

        public Task CerrarSesion(string token)
        {
            return _httpContextAccessor.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public Task<FirebaseAuthLink> RegistrarUsuario(UsuarioDto usuarioDto)
        {
            try
            {
                return _usuarioDao.RegistrarUsuario(usuarioDto, usuarioDto.Contrasena);
            }
            catch (FirebaseAuthException e)
            {
                Console.WriteLine(e.Reason.ToString());
                throw;
            }
        }


        public Task<UsuarioModel> ActualizarUsuario(UsuarioDto usuarioDto)
        {
            try
            {
                return  _usuarioDao.ActualizarUsuario(usuarioDto, usuarioDto.Contrasena,usuarioDto.Token);
            }
            catch (FirebaseAuthException e)
            {
                Console.WriteLine(e.Reason.ToString());
                throw;
            }
        }
    }
}
