﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Services.Interfaces
{
    public interface ICalificacionProductoService
    {
        public Task<decimal> ConsultarCalificacionProducto(string productoId);
    }
}
