﻿using FastshippingCar.Web.Models;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Services.Interfaces
{
    public interface IRolService
    {
        Task<RolesModel> RolUsuario();
    }
}
