﻿using FastshippingCar.Web.Dto;
using FastshippingCar.Web.Models;
using Firebase.Auth;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Services.Interfaces
{
    public interface IUsuarioService
    {
        Task<FirebaseAuthLink> AutenticarUsuario(string email, string contrasena);

        Task<FirebaseAuthLink> RegistrarUsuario(UsuarioDto usuarioDto);

        Task CerrarSesion(string token);

        Task<UsuarioModel> ActualizarUsuario(UsuarioDto usuarioDto);
    }
}
