﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Services.Interfaces
{
    public interface IReputacionVendedorService
    {
        public Task<decimal> ConsultarReputacionVendedor(string tiendaId);
    }
}
