﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Models.Dao.Intefaces
{
    public interface IServiceGenerico<T> where T : ModelGenerico
    {
        Task<List<T>> Consultar();

        Task<T> Tomar(string id);

        Task<T> Crear(T dato);
        Task Actualizar(string id, T dato);
        Task Borrar(string id);
    }
}
