﻿using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Firebase.Auth;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Services
{
    public class ServiceGenericoImpl<T> : IServiceGenerico<T> where T : ModelGenerico
    {
        private readonly IDaoGenerico<T> _daoGenerico;

        public ServiceGenericoImpl(IDaoGenerico<T> productoDao)
        {
            _daoGenerico = productoDao;
        }

        public Task<List<T>> Consultar()
        {
            try
            {
                return _daoGenerico.Consultar();
            }
            catch (FirebaseAuthException e)
            {
                Console.WriteLine(e.Reason.ToString());
                throw;
            }
        }

        public Task<T> Tomar(string id)
        {
            try
            {
                return _daoGenerico.Tomar(id);
            }
            catch (FirebaseAuthException e)
            {
                Console.WriteLine(e.Reason.ToString());
                throw;
            }
        }

        public Task<T> Crear(T dato)
        {
            try
            {
                return _daoGenerico.Crear(dato);
            }
            catch (FirebaseAuthException e)
            {
                Console.WriteLine(e.Reason.ToString());
                throw;
            }
        }

        public Task Actualizar(string id, T dato)
        {
            try
            {
                return _daoGenerico.Actualizar(id, dato);
            }
            catch (FirebaseAuthException e)
            {
                Console.WriteLine(e.Reason.ToString());
                throw;
            }
        }

        public Task Borrar(string id)
        {
            try
            {
                return _daoGenerico.Borrar(id);
            }
            catch (FirebaseAuthException e)
            {
                Console.WriteLine(e.Reason.ToString());
                throw;
            }
        }
    }
}
