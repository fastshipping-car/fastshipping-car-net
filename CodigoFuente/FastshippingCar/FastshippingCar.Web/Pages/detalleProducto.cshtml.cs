﻿using FastshippingCar.Web.Common;
using FastshippingCar.Web.Dto;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Pages
{
    public class detalleProductoModel : PageModel
    {
        private readonly ILogger<detalleProductoModel> _logger;
        private readonly IServiceGenerico<ProductoModel> _productoService;
        private readonly IServiceGenerico<CarritoCompraModel> _carritoCompra;
        private readonly IServiceGenerico<UsuarioModel> _usuario;
        private readonly IServiceGenerico<VendedorModel> _vendedor;
        private readonly IServiceGenerico<TiendaModel> _tienda;

        public detalleProductoModel(ILogger<detalleProductoModel> logger
            , IServiceGenerico<ProductoModel> productoService
            , IServiceGenerico<CarritoCompraModel> carritoCompra
            , IServiceGenerico<UsuarioModel> usuario
            , IServiceGenerico<VendedorModel> vendedor
            , IServiceGenerico<TiendaModel> tienda)
        {
            _logger = logger;
            _productoService = productoService;
            _carritoCompra = carritoCompra;
            _usuario = usuario;
            _vendedor = vendedor;
            _tienda = tienda;
        }
        [BindProperty]
        public ContactarVendedorDto ContactarVendedor { get; set; }
        public VendedorDto Vendedor { get; set; }
        [BindProperty]
        public ProductoModel Producto { get; set; }
        public async Task OnGetAsync(string id)
        {
            Producto = await _productoService.Tomar(id);
            await CargarVendedor();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            Producto = await _productoService.Tomar(Producto.Id);
            await CargarVendedor();
            var dato = new CarritoCompraModel
            {
                UsuarioId = Helpers.UserId(User),
                ProductoId = Producto.Id,
                Pagado = false
            };
            await _carritoCompra.Crear(dato);
            return RedirectToPage("/misProductos");
        }

        public async Task<IActionResult> OnPostAdicionarFavoritoAsync([FromServices] IServiceGenerico<ProductoFavoritoModel> productoFavorito)
        {
            var dato = new ProductoFavoritoModel
            {
                UsuarioId = Helpers.UserId(User),
                ProductoId = Producto.Id
            };
            await productoFavorito.Crear(dato);
            return RedirectToPage("/detalleProducto", new { id = Producto.Id });
        }

        private async Task CargarVendedor()
        {
            var tienda = await _tienda.Tomar(Producto.TiendaId);
            var vendedor = await _vendedor.Tomar(tienda.VendedorId);
            if (vendedor != null)
            {
                var usuario = await _usuario.Tomar(vendedor.UsuarioId);
                Vendedor = new VendedorDto
                {
                    Vendedor = vendedor,
                    Usuario = usuario
                };
                ContactarVendedor = new ContactarVendedorDto
                {
                    VendedorId = tienda.VendedorId
                };
            }

        }
    }
}