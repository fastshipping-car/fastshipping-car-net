﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FastshippingCar.Web.Common;
using FastshippingCar.Web.Dto;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Models.Enums;
using FastshippingCar.Web.Settings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;

namespace FastshippingCar.Web.Pages
{
    public class misProductosModel : PageModel
    {
        private readonly IServiceGenerico<CarritoCompraModel> _carritoCompra;
        private readonly IServiceGenerico<ProductoModel> _producto;
        

        public misProductosModel(IServiceGenerico<CarritoCompraModel> carritoCompra
            , IServiceGenerico<ProductoModel> producto)
        {
            _carritoCompra = carritoCompra;
            _producto = producto;
            Productos = new List<ProductoModel>();
        }

        [BindProperty]
        public List<ProductoModel> Productos { get; set; }
        [BindProperty]
        public FormularioPagoDto FormularioPago { get; set; }

        public async Task OnGetAsync([FromServices] IOptions<PayUConfiguration> payU)
        {
            var compras = await _carritoCompra.Consultar();
            foreach (var compra in compras.Where(x => x.UsuarioId == Helpers.UserId(User) && !x.Pagado))
            {
                Productos.Add(await _producto.Tomar(compra.ProductoId));
            }
            if (Productos != null && Productos.Any())
            {
                FormularioPago = new FormularioPagoDto
                {
                    Description = "Pagos - Fastshipping Car",
                    ReferenceCode = Guid.NewGuid().ToString().Replace("-", ""),
                    Amount = Productos.Sum(x => (long)(x.Precio - (x.Precio * ((decimal)x.Porcentaje / 100)))),
                    Tax = 0,
                    TaxReturnBase = 0,
                };
                FormularioPago.CalcularSignature(payU.Value);
            }
        }

        
    }
}