﻿using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Pages
{
    public class CrearPreguntaModel : PageModel
    {
        [BindProperty]
        public PqrsProductoModel PqrsPregunta { get; set; }
        public async Task<IActionResult> OnPostAsync([FromServices] IServiceGenerico<PqrsProductoModel> pqrsPregunta)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await pqrsPregunta.Crear(PqrsPregunta);
            return RedirectToPage("/EditarPregunta");
        }
    }
}