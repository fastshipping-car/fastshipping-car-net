﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FastshippingCar.Web.Pages
{
    public class EliminarPreguntaModel : PageModel
    {
        public async Task<IActionResult> OnGetAsync([FromServices] IServiceGenerico<PqrsProductoModel> pqrsPregunta, string id)
        {
            await pqrsPregunta.Borrar(id);
            return RedirectToPage("/EditarPregunta");
        }
    }
}