﻿using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Pages
{
    public class EditarPreguntaModel : PageModel
    {
        private readonly IServiceGenerico<PqrsProductoModel> _pqrsProducto;

        public EditarPreguntaModel(IServiceGenerico<PqrsProductoModel> pqrsProducto)
        {
            _pqrsProducto = pqrsProducto;
        }

        public List<PqrsProductoModel> PqrsPreguntas { get; set; }
        public async Task OnGetAsync()
        {
            PqrsPreguntas = await _pqrsProducto.Consultar();
        }
    }
}