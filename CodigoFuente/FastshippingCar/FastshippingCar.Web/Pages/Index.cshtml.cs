﻿using FastshippingCar.Web.Dto;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IServiceGenerico<ProductoModel> _productoService;

        public IndexModel(ILogger<IndexModel> logger
            , IServiceGenerico<ProductoModel> productoService)
        {
            _logger = logger;
            _productoService = productoService;
        }

        public List<ProductoDto> Productos { get; set; }
        public async Task OnGetAsync([FromServices] ICalificacionProductoService calificacionProductoService)
        {
            Productos = new List<ProductoDto>();
            var prodcutos = await _productoService.Consultar();
            foreach (var producto in prodcutos)
            {
                Productos.Add(new ProductoDto
                {
                    Producto = producto,
                    ProductoPuntuacion = await calificacionProductoService.ConsultarCalificacionProducto(producto.Id)
                });
            }
        }
    }
}
