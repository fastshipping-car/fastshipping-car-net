﻿using FastshippingCar.Web.Common;
using FastshippingCar.Web.Dto;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Context;
using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Models.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Pages
{
    public class informacionPagoModel : PageModel
    {
        private readonly IServiceGenerico<CarritoCompraModel> _carritoCompra;
        private readonly IServiceGenerico<ProductoModel> _producto;


        public informacionPagoModel(IServiceGenerico<CarritoCompraModel> carritoCompra
            , IServiceGenerico<ProductoModel> producto)
        {
            _carritoCompra = carritoCompra;
            _producto = producto;
        }

        public List<CarritoCompraModel> CarritoCompras { get; set; }
        public List<ProductoModel> Productos { get; set; }
        [BindProperty]
        public FormularioPagoDto FormularioPago { get; set; }

        public async Task OnPostAsync()
        {
            CarritoCompras = new List<CarritoCompraModel>();
            Productos = new List<ProductoModel>();
            var compras = await _carritoCompra.Consultar();
            foreach (var compra in compras.Where(x => x.UsuarioId == Helpers.UserId(User) && !x.Pagado))
            {
                CarritoCompras.Add(compra);
                Productos.Add(await _producto.Tomar(compra.ProductoId));
            }
            FormularioPago.SendCost = 10000;
        }

        public async Task<IActionResult> OnPostPagarAsync([FromServices] SendGridEmail sendGrid
            , [FromServices] IServiceGenerico<UsuarioModel> usuario
            , [FromServices] IServiceGenerico<TiendaModel> tienda
            , [FromServices] IServiceGenerico<HistorialCompraModel> historialCompra
            , [FromForm] string[] idCarrito)
        {
            var infoUsuario = await usuario.Tomar(Helpers.UserId(User));
            foreach (var carrito in idCarrito)
            {
                var dato = await _carritoCompra.Tomar(carrito);
                var infoTienda = await tienda.Tomar(dato.TiendaId);
                var vendedor = await usuario.Tomar(infoTienda.VendedorId);
                var producto = await _producto.Tomar(dato.ProductoId);
                dato.Pagado = true;
                await _carritoCompra.Actualizar(carrito, dato);
                var historial = new HistorialCompraModel
                {
                    TiendaId = infoTienda.Id,
                    UsuarioId = Helpers.UserId(User),
                    ProductoId = producto.Id,
                    Producto = producto,
                    EstadoCompra = EstadoCompra.PendienteEnvio
                };
                await historialCompra.Crear(historial);
                await sendGrid.SendMail("Cliente - Compra de productos", $"Producto comprado: {dato.ProductoId}", infoUsuario.Email, vendedor.Email);
                await sendGrid.SendMail("Vendedor - Compra de productos", $"Producto comprado: {dato.ProductoId}", vendedor.Email, vendedor.Email);
            }
            return RedirectToPage("/misProductos");
        }
    }
}