﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FastshippingCar.Web.Common;
using FastshippingCar.Web.Dto;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Models.Enums;
using FastshippingCar.Web.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FastshippingCar.Web.Pages
{
    public class MisPedidosModel : PageModel
    {
        private readonly IServiceGenerico<HistorialCompraModel> _historialCompra;
        private readonly IServiceGenerico<ProductoModel> _producto;


        public MisPedidosModel(IServiceGenerico<HistorialCompraModel> historialCompra
            , IServiceGenerico<ProductoModel> producto)
        {
            _historialCompra = historialCompra;
            _producto = producto;
            HistorialProductos = new List<HistorialCompraDto>();
        }

        [BindProperty]
        public List<HistorialCompraDto> HistorialProductos { get; set; }
        [BindProperty]
        public HistorialCompraModel HistorialCompra { get; set; }

        public async Task OnGetAsync([FromServices] ICalificacionProductoService calificacionProductoService)
        {
            HistorialProductos = new List<HistorialCompraDto>();
            var compras = await _historialCompra.Consultar();
            var historiales = compras.Where(x => x.UsuarioId == Helpers.UserId(User) && x.EstadoCompra != EstadoCompra.PendientePago).ToList();
            foreach (var historial in historiales)
            {
                HistorialProductos.Add(new HistorialCompraDto
                {
                    HistorialCompra = historial,
                    ProductoPuntuacion = await calificacionProductoService.ConsultarCalificacionProducto(historial.Producto.Id)
                });
            }
        }

        public async Task<IActionResult> OnPostCancelarAsync()
        {
            var compra = await _historialCompra.Tomar(HistorialCompra.Id);
            compra.EstadoCompra = EstadoCompra.PagoCancelado;
            await _historialCompra.Actualizar(compra.Id, compra);
            return RedirectToPage("/MisPedidos");
        }

        public async Task<IActionResult> OnGetCalificarAsync([FromServices] IServiceGenerico<ProductoPuntuacionModel> productoPuntuacion, [FromQuery] string id, [FromQuery] int valor)
        {
            var puntuacion = new ProductoPuntuacionModel
            {
                ProductoId = id,
                UsuarioId = Helpers.UserId(User),
                ValorPuntuacion = valor
            };
            await productoPuntuacion.Crear(puntuacion);
            return RedirectToPage("/MisPedidos");
        }
    }
}