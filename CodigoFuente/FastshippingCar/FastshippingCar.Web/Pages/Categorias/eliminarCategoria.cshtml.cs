﻿using System.Threading.Tasks;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FastshippingCar.Web.Pages.Categorias
{
    public class EliminarCategoriaModel : PageModel
    {
        public async Task<IActionResult> OnGetAsync([FromServices] IServiceGenerico<CategoriaModel> categoriaService, string id)
        {
            await categoriaService.Borrar(id);
            return RedirectToPage("/Categorias/editarCategoria");
        }
    }
}