﻿using System.Threading.Tasks;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FastshippingCar.Web.Pages.Categorias
{
    public class CrearCategoriaModel : PageModel
    {
        [BindProperty]
        public CategoriaModel Categoria { get; set; }
        public async Task<IActionResult> OnPostAsync([FromServices] IServiceGenerico<CategoriaModel> categoriaService)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await categoriaService.Crear(Categoria);
            return RedirectToPage("/Categorias/editarCategoria");
        }
    }
}