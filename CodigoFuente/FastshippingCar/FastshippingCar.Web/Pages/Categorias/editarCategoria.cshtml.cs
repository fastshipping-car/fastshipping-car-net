﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FastshippingCar.Web.Pages
{
    public class EditarCategoriaModel : PageModel
    {
        private readonly IServiceGenerico<CategoriaModel> _categoriaService;

        public EditarCategoriaModel(IServiceGenerico<CategoriaModel> categoriaService)
        {
            _categoriaService = categoriaService;
        }

        public List<CategoriaModel> Categorias { get; set; }
        public async Task OnGetAsync()
        {
            Categorias = await _categoriaService.Consultar();
        }
    }
}