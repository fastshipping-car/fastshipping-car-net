﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FastshippingCar.Web.Pages.Categorias
{
    public class actualizarCategoriaModel : PageModel
    {
        private readonly IServiceGenerico<CategoriaModel> _categoriaService;

        public actualizarCategoriaModel(IServiceGenerico<CategoriaModel> categoriaService)
        {
            _categoriaService = categoriaService;
        }

        [BindProperty]
        public CategoriaModel Categoria { get; set; }
        public async Task OnGetAsync(string id)
        {
            Categoria = await _categoriaService.Tomar(id);
        }

        public async Task<IActionResult> OnPostAsync(string id)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await _categoriaService.Actualizar(id, Categoria);
            return RedirectToPage("/Categorias/editarCategoria");
        }
    }
}