﻿using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Pages
{
    public class PreguntasFrecuentesModel : PageModel
    {
        private readonly IServiceGenerico<PqrsProductoModel> _pqrsProducto;

        public PreguntasFrecuentesModel(IServiceGenerico<PqrsProductoModel> pqrsProducto)
        {
            _pqrsProducto = pqrsProducto;
        }

        public List<PqrsProductoModel> PqrsProductos { get; set; }
        public async Task OnGetAsync()
        {
            PqrsProductos = await _pqrsProducto.Consultar();
        }
    }
}