﻿using FastshippingCar.Web.Dto;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Context;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Pages
{
    public class ContactarVendedorModel : PageModel
    {
        private readonly ILogger<detalleProductoModel> _logger;
        private readonly IServiceGenerico<UsuarioModel> _usuario;
        private readonly IServiceGenerico<VendedorModel> _vendedor;
        private readonly SendGridEmail _sendGrid;

        public ContactarVendedorModel(ILogger<detalleProductoModel> logger
            , IServiceGenerico<UsuarioModel> usuario
            , IServiceGenerico<VendedorModel> vendedor
            , SendGridEmail sendGrid)
        {
            _logger = logger;
            _usuario = usuario;
            _vendedor = vendedor;
            _sendGrid = sendGrid;
        }

        [BindProperty]
        public ContactarVendedorDto ContactarVendedor { get; set; }
        public async Task<IActionResult> OnPostAsync(string id)
        {
            var vendedor = await _vendedor.Tomar(ContactarVendedor.VendedorId);
            if (vendedor != null)
            {
                var usuario = await _usuario.Tomar(vendedor.UsuarioId);
                await _sendGrid.SendMail(ContactarVendedor.Asunto, ContactarVendedor.Mensaje, usuario.Email, usuario.Email);
            }
            return RedirectToPage("/detalleProducto", new { id });
        }

    }
}