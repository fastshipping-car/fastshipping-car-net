﻿using System.Threading.Tasks;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FastshippingCar.Web.Pages.Tiendas
{
    public class EliminarTiendaModel : PageModel
    {
        public async Task<IActionResult> OnGetAsync([FromServices] IServiceGenerico<TiendaModel> tiendaService, string id)
        {
            await tiendaService.Borrar(id);
            return RedirectToPage("/Tiendas/editarTienda");
        }
    }
}