﻿using System.Threading.Tasks;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FastshippingCar.Web.Pages.Tiendas
{
    public class CrearTiendaModel : PageModel
    {
        [BindProperty]
        public TiendaModel Tienda { get; set; }
        public async Task<IActionResult> OnPostAsync([FromServices] IServiceGenerico<TiendaModel> tiendaService)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await tiendaService.Crear(Tienda);
            return RedirectToPage("/Tiendas/editarTienda");
        }
    }
}