﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FastshippingCar.Web.Pages
{
    public class EditarTiendaModel : PageModel
    {
        private readonly IServiceGenerico<TiendaModel> _tiendaService;

        public EditarTiendaModel(IServiceGenerico<TiendaModel> tiendaService)
        {
            _tiendaService = tiendaService;
        }

        public List<TiendaModel> Tiendas { get; set; }
        public async Task OnGetAsync()
        {
            Tiendas = await _tiendaService.Consultar();
        }
    }
}