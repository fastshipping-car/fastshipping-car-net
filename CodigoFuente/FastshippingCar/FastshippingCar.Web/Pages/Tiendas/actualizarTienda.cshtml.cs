﻿using System.Threading.Tasks;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FastshippingCar.Web.Pages.Tiendas
{
    public class ActualizarTiendaModel : PageModel
    {
        private readonly IServiceGenerico<TiendaModel> _tiendaService;

        public ActualizarTiendaModel(IServiceGenerico<TiendaModel> tiendaService)
        {
            _tiendaService = tiendaService;
        }

        [BindProperty]
        public TiendaModel Tienda { get; set; }
        public async Task OnGetAsync(string id)
        {
            Tienda = await _tiendaService.Tomar(id);
        }

        public async Task<IActionResult> OnPostAsync(string id)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await _tiendaService.Actualizar(id, Tienda);
            return RedirectToPage("/Tiendas/editarTienda");
        }
    }
}