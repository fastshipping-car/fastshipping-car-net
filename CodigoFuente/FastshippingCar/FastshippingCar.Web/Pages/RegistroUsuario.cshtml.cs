﻿using FastshippingCar.Web.Dto;
using FastshippingCar.Web.Models.Enums;
using FastshippingCar.Web.Services.Interfaces;
using Firebase.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Pages
{
    public class RegistroUsuarioModel : PageModel
    {
        private readonly IUsuarioService _usuarioService;

        public RegistroUsuarioModel(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }


        

        [BindProperty]
        public UsuarioDto Usuario { get; set; }
        public async Task<IActionResult> OnPostAsync()
        {

            if (!ModelState.IsValid)
            {
                return Page();
            }


            try
            {
                Usuario.RolId = Roles.CLI.ToString();
                await _usuarioService.RegistrarUsuario(Usuario);
                TempData["msg"] = "<script>alert('Usuario registrado correctamente.');</script>";
                return RedirectToPage("./Login");
            }
            catch (FirebaseAuthException e)
            {
                switch (e.Reason)
                {
                    case AuthErrorReason.Undefined:
                        TempData["msg"] = "<script>alert('Indefinido.');</script>";
                        break;
                    case AuthErrorReason.OperationNotAllowed:
                        TempData["msg"] = "<script>alert('Operación no permitida.');</script>";
                        break;
                    case AuthErrorReason.UserDisabled:
                        TempData["msg"] = "<script>alert('Usuario deshabilitado.');</script>";
                        break;
                    case AuthErrorReason.UserNotFound:
                        TempData["msg"] = "<script>alert('Usuario no encontrado.');</script>";
                        break;
                    case AuthErrorReason.InvalidProviderID:
                        TempData["msg"] = "<script>alert('ID de proveedor no válido.');</script>";
                        break;
                    case AuthErrorReason.InvalidAccessToken:
                        TempData["msg"] = "<script>alert('Token de acceso inválido.');</script>";
                        break;
                    case AuthErrorReason.LoginCredentialsTooOld:
                        TempData["msg"] = "<script>alert('Credenciales de inicio de sesión demasiado viejas.');</script>";
                        break;
                    case AuthErrorReason.MissingRequestURI:
                        TempData["msg"] = "<script>alert('URI de solicitud faltante.');</script>";
                        break;
                    case AuthErrorReason.SystemError:
                        TempData["msg"] = "<script>alert('Error del sistema.');</script>";
                        break;
                    case AuthErrorReason.InvalidEmailAddress:
                        TempData["msg"] = "<script>alert('Dirección de correo electrónico no válida.');</script>";
                        break;
                    case AuthErrorReason.MissingPassword:
                        TempData["msg"] = "<script>alert('Digite la contraseña.');</script>";
                        break;
                    case AuthErrorReason.WeakPassword:
                        TempData["msg"] = "<script>alert('Contraseña débil.');</script>";
                        break;
                    case AuthErrorReason.EmailExists:
                        TempData["msg"] = "<script>alert('Existe el email.');</script>";
                        break;
                    case AuthErrorReason.MissingEmail:
                        TempData["msg"] = "<script>alert('Email no encontrado.');</script>";
                        break;
                    case AuthErrorReason.UnknownEmailAddress:
                        TempData["msg"] = "<script>alert('Dirección de Email desconocida.');</script>";
                        break;
                    case AuthErrorReason.WrongPassword:
                        TempData["msg"] = "<script>alert('Contraseña incorrecta.');</script>";
                        break;
                    case AuthErrorReason.TooManyAttemptsTryLater:
                        TempData["msg"] = "<script>alert('Demasiados intentos,por favor intente más tarde.');</script>";
                        break;
                    case AuthErrorReason.MissingRequestType:
                        TempData["msg"] = "<script>alert('Falta el tipo de solicitud');</script>";
                        break;
                    case AuthErrorReason.ResetPasswordExceedLimit:
                        TempData["msg"] = "<script>alert('Restablecer contraseña, excedió el límite.');</script>";
                        break;
                    case AuthErrorReason.InvalidIDToken:
                        TempData["msg"] = "<script>alert('Token de ID inválido');</script>";
                        break;
                    case AuthErrorReason.MissingIdentifier:
                        TempData["msg"] = "<script>alert('Identificador faltante.');</script>";
                        break;
                    case AuthErrorReason.InvalidIdentifier:
                        TempData["msg"] = "<script>alert('Identificador no válido.');</script>";
                        break;
                    case AuthErrorReason.AlreadyLinked:
                        TempData["msg"] = "<script>alert('Ya se encuentra la cuenta vinculada.');</script>";
                        break;
                    default:
                        break;
                }


                return Page();

            }
        }
    }
}