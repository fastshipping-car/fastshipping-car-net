﻿using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Pages
{
    public class ActualizarPreguntaModel : PageModel
    {
        private readonly IServiceGenerico<PqrsProductoModel> _pqrsPregunta;

        public ActualizarPreguntaModel(IServiceGenerico<PqrsProductoModel> pqrsPregunta)
        {
            _pqrsPregunta = pqrsPregunta;
        }

        [BindProperty]
        public PqrsProductoModel PqrsPregunta { get; set; }
        public async Task OnGetAsync(string id)
        {
            PqrsPregunta = await _pqrsPregunta.Tomar(id);
        }

        public async Task<IActionResult> OnPostAsync(string id)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await _pqrsPregunta.Actualizar(id, PqrsPregunta);
            return RedirectToPage("/EditarPregunta");
        }
    }
}