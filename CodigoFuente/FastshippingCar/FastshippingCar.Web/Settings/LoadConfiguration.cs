﻿using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Services.Interfaces;
using FastshippingCar.Web.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FastshippingCar.Web.Services
{
    public static class LoadConfiguration
    {
        public static void CargarConfiguracion(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<FirebaseConfiguration>(configuration.GetSection("Firebase"));
            services.Configure<PayUConfiguration>(configuration.GetSection("PayU"));
            services.Configure<SendGridConfiguration>(configuration.GetSection("SendGrid"));
        }
    }
}
