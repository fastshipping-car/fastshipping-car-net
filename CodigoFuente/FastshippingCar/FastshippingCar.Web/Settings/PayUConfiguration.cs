﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Settings
{
    public class PayUConfiguration
    {
        public string ApiKey { get; set; }
        public int MerchantId { get; set; }
        public int AccountId { get; set; }
        public string Currency { get; set; }
        public bool Test { get; set; }
    }
}
