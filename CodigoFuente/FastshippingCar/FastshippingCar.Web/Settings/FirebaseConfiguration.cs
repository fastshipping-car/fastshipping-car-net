﻿namespace FastshippingCar.Web.Settings
{
    public class FirebaseConfiguration
    {
        public string ApiKey { get; set; }
        public string RealtimeDatabaseUrl { get; set; }
        public string FirebaseServiceAccountFilename { get; set; }
    }
}
