﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace FastshippingCar.Web.Models
{
    public class OpcionesModel  : ModelGenerico
    {
        public string Descripcion { get; set; }
        public string Url { get; set; }
        public List<OpcionesModel> Opciones { get; set; }
        [JsonIgnore]
        public override string Id { get; set; }
    }
}
