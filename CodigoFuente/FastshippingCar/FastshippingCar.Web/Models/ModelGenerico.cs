﻿using Newtonsoft.Json;

namespace FastshippingCar.Web.Models
{
    public abstract class ModelGenerico
    {
        [JsonIgnore]
        public abstract string Id { get; set; }
    }
}
