﻿using FastshippingCar.Web.Common;
using System.ComponentModel.DataAnnotations;


namespace FastshippingCar.Web.Models
{
    
    [NombreDocumento(Name = "tiendas")]
    public class TiendaModel : ModelGenerico
    {
        [Required(ErrorMessage = "El {0} es requerido")]
        [DataType(DataType.Text)]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "El {0} es requerido")]
        [DataType(DataType.MultilineText)]
        public string Ciudad { get; set; }

        [Required(ErrorMessage = "El {0} es requerido")]
        [DataType(DataType.MultilineText)]
        public string Direccion { get; set; }

        [Required(ErrorMessage = "El {0} es requerido")]
        [DataType(DataType.MultilineText)]
        public string Telefono { get; set; }
        public string VendedorId { get; set; }

        public override string Id { get; set; }
    }

}
