﻿using FastshippingCar.Web.Common;
using FastshippingCar.Web.Models.Enums;
using Newtonsoft.Json;

namespace FastshippingCar.Web.Models
{
    [NombreDocumento(Name = "historial-comprar")]
    public class HistorialCompraModel : ModelGenerico
    {
        public string UsuarioId { get; set; }
        public ProductoModel Producto { get; set; }
        public string TiendaId { get; set; }
        public EstadoCompra EstadoCompra { get; set; }
        [JsonIgnore]
        public override string Id { get; set; }
        public string ProductoId { get; internal set; }
    }
}
