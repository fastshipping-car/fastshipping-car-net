﻿using FastshippingCar.Web.Common;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace FastshippingCar.Web.Models
{
    [NombreDocumento(Name = "usuarios")]
    public class UsuarioModel : ModelGenerico
    {
        [Required(ErrorMessage = "El {0} es requerido")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email is not valid.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        [Required(ErrorMessage = "El {0} es requerido")]
        [DataType(DataType.Text)]
        //[StringLength(60, MinimumLength = 5)]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El {0} es requerido")]
        [DataType(DataType.Text)]
        //[StringLength(60, MinimumLength = 5)]
        public string Apellido { get; set; }

        [Required(ErrorMessage = "El {0} es requerido")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "número documento")]
        [StringLength(10, MinimumLength = 8, ErrorMessage = "El {0} debe estar entre {2} a {1} caracteres")]
        public string NumeroDocumento { get; set; }

        [Required(ErrorMessage = "El {0} es requerido")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "teléfono")]
        [StringLength(10, MinimumLength = 7, ErrorMessage = "El {0} debe estar entre {2} a {1} caracteres")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "La {0} es requerida")]
        [DataType(DataType.Text)]
        [Display(Name = "dirección")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "El {0} debe estar entre {2} a {1} caracteres")]
        public string Direccion { get; set; }


        public string RolId { get; set; }

        [JsonIgnore]
        public string Token { get; set; }

        [JsonIgnore]
        public override string Id { get; set; }

        
    }
}
