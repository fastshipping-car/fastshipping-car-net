﻿using System.Text.Json.Serialization;

namespace FastshippingCar.Web.Models
{

    public class Merchant
    {

        [JsonPropertyName("apiKey")]
        public string ApiKey { get; set; }

        [JsonPropertyName("apiLogin")]
        public string ApiLogin { get; set; }
    }

    public class TxValue
    {

        [JsonPropertyName("value")]
        public int Value { get; set; }

        [JsonPropertyName("currency")]
        public string Currency { get; set; }
    }

    public class TxTax
    {

        [JsonPropertyName("value")]
        public int Value { get; set; }

        [JsonPropertyName("currency")]
        public string Currency { get; set; }
    }

    public class TxTaxReturnBase
    {

        [JsonPropertyName("value")]
        public int Value { get; set; }

        [JsonPropertyName("currency")]
        public string Currency { get; set; }
    }

    public class AdditionalValues
    {

        [JsonPropertyName("TX_VALUE")]
        public TxValue TXVALUE { get; set; }

        [JsonPropertyName("TX_TAX")]
        public TxTax TXTAX { get; set; }

        [JsonPropertyName("TX_TAX_RETURN_BASE")]
        public TxTaxReturnBase TXTAXRETURNBASE { get; set; }
    }

    public class ShippingAddress
    {

        [JsonPropertyName("street1")]
        public string Street1 { get; set; }

        [JsonPropertyName("street2")]
        public string Street2 { get; set; }

        [JsonPropertyName("city")]
        public string City { get; set; }

        [JsonPropertyName("state")]
        public string State { get; set; }

        [JsonPropertyName("country")]
        public string Country { get; set; }

        [JsonPropertyName("postalCode")]
        public string PostalCode { get; set; }

        [JsonPropertyName("phone")]
        public string Phone { get; set; }
    }

    public class Buyer
    {

        [JsonPropertyName("merchantBuyerId")]
        public string MerchantBuyerId { get; set; }

        [JsonPropertyName("fullName")]
        public string FullName { get; set; }

        [JsonPropertyName("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonPropertyName("contactPhone")]
        public string ContactPhone { get; set; }

        [JsonPropertyName("dniNumber")]
        public string DniNumber { get; set; }

        [JsonPropertyName("shippingAddress")]
        public ShippingAddress ShippingAddress { get; set; }
    }

    public class Order
    {

        [JsonPropertyName("accountId")]
        public string AccountId { get; set; }

        [JsonPropertyName("referenceCode")]
        public string ReferenceCode { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("language")]
        public string Language { get; set; }

        [JsonPropertyName("signature")]
        public string Signature { get; set; }

        [JsonPropertyName("notifyUrl")]
        public string NotifyUrl { get; set; }

        [JsonPropertyName("additionalValues")]
        public AdditionalValues AdditionalValues { get; set; }

        [JsonPropertyName("buyer")]
        public Buyer Buyer { get; set; }

        [JsonPropertyName("shippingAddress")]
        public ShippingAddress ShippingAddress { get; set; }
    }

    public class BillingAddress
    {

        [JsonPropertyName("street1")]
        public string Street1 { get; set; }

        [JsonPropertyName("street2")]
        public string Street2 { get; set; }

        [JsonPropertyName("city")]
        public string City { get; set; }

        [JsonPropertyName("state")]
        public string State { get; set; }

        [JsonPropertyName("country")]
        public string Country { get; set; }

        [JsonPropertyName("postalCode")]
        public string PostalCode { get; set; }

        [JsonPropertyName("phone")]
        public string Phone { get; set; }
    }

    public class Payer
    {

        [JsonPropertyName("merchantPayerId")]
        public string MerchantPayerId { get; set; }

        [JsonPropertyName("fullName")]
        public string FullName { get; set; }

        [JsonPropertyName("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonPropertyName("contactPhone")]
        public string ContactPhone { get; set; }

        [JsonPropertyName("dniNumber")]
        public string DniNumber { get; set; }

        [JsonPropertyName("billingAddress")]
        public BillingAddress BillingAddress { get; set; }
    }

    public class CreditCard
    {

        [JsonPropertyName("number")]
        public string Number { get; set; }

        [JsonPropertyName("securityCode")]
        public string SecurityCode { get; set; }

        [JsonPropertyName("expirationDate")]
        public string ExpirationDate { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }
    }

    public class ExtraParameters
    {

        [JsonPropertyName("INSTALLMENTS_NUMBER")]
        public int INSTALLMENTSNUMBER { get; set; }
    }

    public class Transaction
    {

        [JsonPropertyName("order")]
        public Order Order { get; set; }

        [JsonPropertyName("payer")]
        public Payer Payer { get; set; }

        [JsonPropertyName("creditCard")]
        public CreditCard CreditCard { get; set; }

        [JsonPropertyName("extraParameters")]
        public ExtraParameters ExtraParameters { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("paymentMethod")]
        public string PaymentMethod { get; set; }

        [JsonPropertyName("paymentCountry")]
        public string PaymentCountry { get; set; }

        [JsonPropertyName("deviceSessionId")]
        public string DeviceSessionId { get; set; }

        [JsonPropertyName("ipAddress")]
        public string IpAddress { get; set; }

        [JsonPropertyName("cookie")]
        public string Cookie { get; set; }

        [JsonPropertyName("userAgent")]
        public string UserAgent { get; set; }
    }

    public class PayURequest
    {

        [JsonPropertyName("language")]
        public string Language { get; set; }

        [JsonPropertyName("command")]
        public string Command { get; set; }

        [JsonPropertyName("merchant")]
        public Merchant Merchant { get; set; }

        [JsonPropertyName("transaction")]
        public Transaction Transaction { get; set; }

        [JsonPropertyName("test")]
        public bool Test { get; set; }
    }

}

