﻿using FastshippingCar.Web.Common;
using System.Collections.Generic;

namespace FastshippingCar.Web.Models
{
    [NombreDocumento(Name = "productos")]
    public class ProductoModel : ModelGenerico
    {
        public override string Id { get; set; }
        public string Nombre { get; set; }
        public string Resumen { get; set; }
        public short Porcentaje { get; set; }
        public long Precio { get; set; }
        public List<ProductoCaracteristicaModel> Caracteristicas { get; set; }
        public List<ProductoDetalleModel> ProductoDetalles { get; set; }
        public List<string> Imagenes { get; set; }
        public string TiendaId { get; set; }
        public List<string> CategoriaId { get; set; }
    }
}
