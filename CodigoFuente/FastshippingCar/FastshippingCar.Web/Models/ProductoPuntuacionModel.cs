﻿using FastshippingCar.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Models
{
    [NombreDocumento("producto-puntuacion")]
    public class ProductoPuntuacionModel : ModelGenerico
    {
        public string UsuarioId { get; set; }
        public string Comentario { get; set; }
        public int ValorPuntuacion { get; set; }
        public string ProductoId { get; set; }
        public override string Id { get; set; }
    }
}
