﻿namespace FastshippingCar.Web.Models
{
    public class ProductoDetalleModel
    {
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
    }
}
