﻿using FastshippingCar.Web.Common;

namespace FastshippingCar.Web.Models
{
    [NombreDocumento("producto-favorito")]
    public class ProductoFavoritoModel : ModelGenerico
    {
        public string ProductoId { get; set; }
        public string UsuarioId { get; set; }
        public override string Id { get; set; }
    }
}
