﻿namespace FastshippingCar.Web.Models.Enums
{
    public enum EstadoCompra
    {
        PendientePago = 1,
        PendienteEnvio,
        PagoCancelado,
        PedidoEnviado,
        Finalizado
    }
}
