﻿namespace FastshippingCar.Web.Models.Enums
{
    public enum Roles
    {
        VEN = 0,
        CLI = 1,
        ADM = 3
    }
}
