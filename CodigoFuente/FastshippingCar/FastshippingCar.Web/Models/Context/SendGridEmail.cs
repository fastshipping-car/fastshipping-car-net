﻿using FastshippingCar.Web.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Models.Context
{
    public class SendGridEmail
    {
        private readonly IOptions<SendGridConfiguration> _sendGrid;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SendGridEmail(IOptions<SendGridConfiguration> sendGrid
            , IHttpContextAccessor httpContextAccessor)
        {
            _sendGrid = sendGrid;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task SendMail(string asunto, string mensaje, string emailCliente, string emailVendedor)
        {
            //var apiKey = Environment.GetEnvironmentVariable("NAME_OF_THE_ENVIRONMENT_VARIABLE_FOR_YOUR_SENDGRID_KEY");
            var apiKey = _sendGrid.Value.ApiKey;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(emailVendedor, "Vendedor");
            var to = new EmailAddress(emailCliente, "Cliente");
            var plainTextContent = mensaje;
            var htmlContent = $"<strong>{mensaje}</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, asunto, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }
    }
}
