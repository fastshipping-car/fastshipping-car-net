﻿using FastshippingCar.Web.Common;
using FastshippingCar.Web.Settings;
using Firebase.Auth;
using Firebase.Database;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.IO;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Models.DbContext
{
    public class FirebaseContext
    {
        private readonly FirebaseConfiguration firebaseConfiguration;
        private readonly IOptions<SendGridConfiguration> _sendGrid;
        public readonly IHttpContextAccessor _httpContextAccessor;
        public FirebaseAuthProvider Context { get; private set; }

        public FirebaseContext(IOptions<FirebaseConfiguration> options
            , IOptions<SendGridConfiguration> sendGrid
            , IHttpContextAccessor httpContextAccessor)
        {
            firebaseConfiguration = options.Value;
            _sendGrid = sendGrid;
            _httpContextAccessor = httpContextAccessor;
            GetContextProvider();
        }

        private void GetContextProvider()
        {
            Context = new FirebaseAuthProvider(new FirebaseConfig(firebaseConfiguration.ApiKey));
        }

        public FirebaseClient GetFirebaseClient(string firebaseToken = null) => new FirebaseClient(
              firebaseConfiguration.RealtimeDatabaseUrl, (!_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated && firebaseToken is null) ? null :
              new FirebaseOptions
              {
                  AuthTokenAsyncFactory = () => Task.FromResult(firebaseToken ?? Helpers.UserToken(_httpContextAccessor.HttpContext.User))
              });

        public FirebaseApp GetFirebaseApp() => FirebaseApp.Create(
            new AppOptions()
            {
                Credential = GoogleCredential.FromFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, firebaseConfiguration.FirebaseServiceAccountFilename)),
                ProjectId = "fastshipping-car",
                //ServiceAccountId = "AAAASSVI73w:APA91bHLSkDwkQZh0tAaRTVqpYSW_LomXWlv5cZIXYEX3dOc9NdV4deKPyrXsvySM30h_B8VjimVjl2TH02QXcl6Yd125Vg4QCzC3g5kKQ63VYksIUO99jrhnzZfrG_7xwYnUprRGsvv"
            });

        public async Task SendMail(string asunto, string mensaje, string emailCliente)
        {
            //var apiKey = Environment.GetEnvironmentVariable("NAME_OF_THE_ENVIRONMENT_VARIABLE_FOR_YOUR_SENDGRID_KEY");
            var apiKey = _sendGrid.Value.ApiKey;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("rol-931@hotmail.com", "Vendedor");
            var to = new EmailAddress(emailCliente, "Cliente");
            var plainTextContent = mensaje;
            var htmlContent = $"<strong>{mensaje}</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, asunto, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }

        public async Task SendNotificationPush(Message message)
        {
            //message.Token = Helpers.UserToken(_httpContextAccessor.HttpContext.User);
            var messaging = FirebaseMessaging.GetMessaging(GetFirebaseApp());
            var result = await messaging.SendAsync(message);
        }

    }
}
