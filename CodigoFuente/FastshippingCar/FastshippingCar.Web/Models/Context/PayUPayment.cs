﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Models.DbContext
{
    public class PayUPayment
    {
        private readonly HttpClient client = new HttpClient();

        public async Task ProcessRepositories()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Content-Type", "application/json");

            var stringTask = client.GetStreamAsync("https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/");
            var dato = await JsonSerializer.DeserializeAsync<List<PayURequest>>(await stringTask);

        }
    }
}
