﻿using Firebase.Database.Query;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Models.Dao.Intefaces
{
    public interface IDaoGenerico<T> where T : ModelGenerico
    {
        public Task<List<T>> Consultar(FilterQuery query = null);
        public Task<T> Tomar(string id);
        public Task<T> Crear(T producto);
        public Task<List<T>> Crear(List<T> producto);
        Task Actualizar(string id, T categoria, bool onlyField = false);
        Task Borrar(string id);
        public ChildQuery ChildQuery { get; }
    }
}
