﻿using Firebase.Auth;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Models.Dao.Intefaces
{
    public interface IUsuarioDao
    {
        Task<FirebaseAuthLink> AutenticarUsuario(string email, string contrasena);

        Task<FirebaseAuthLink> RegistrarUsuario(UsuarioModel usuarioDto, string contrasena);

        Task<UsuarioModel> CrearUsuario(UsuarioModel usuario);

        Task CerrarSesion(string token);

        Task<UsuarioModel> ActualizarUsuario(UsuarioModel usuarioDto, string contrasena,string tokenUser);


        Task<FirebaseAuthLink> CambiarContrasena(string contrasena);
    }
}
