﻿using FastshippingCar.Web.Common;
using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Models.DbContext;
using Firebase.Auth;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Models.Dao
{
    public class UsuarioDaoImpl : IUsuarioDao
    {
        private readonly FirebaseContext _firebaseContext;
        private readonly IDaoGenerico<UsuarioModel> _usuarioDao;
        public UsuarioDaoImpl(FirebaseContext firebaseContext, IDaoGenerico<UsuarioModel> usuarioDao)
        {
            _firebaseContext = firebaseContext;
            _usuarioDao = usuarioDao;
        }

        public async Task<FirebaseAuthLink> AutenticarUsuario(string email, string contrasena)
        {

            var dato = await _firebaseContext.Context.SignInWithEmailAndPasswordAsync(email, contrasena);
                        
            return dato;
        }

        public Task CerrarSesion(string token)
        {
            throw new NotImplementedException();
        }

        public async Task<FirebaseAuthLink> RegistrarUsuario(UsuarioModel usuarioDto, string contrasena)
        {
            var obj = await _firebaseContext.Context.CreateUserWithEmailAndPasswordAsync(usuarioDto.Email, contrasena, usuarioDto.Nombre, true);
            usuarioDto.Id = obj.User.LocalId;

            usuarioDto.Token = obj.FirebaseToken;
            await _firebaseContext.GetFirebaseClient(usuarioDto.Token)
              .Child($"{ConfiguracionAnotacion.GetNombreDocumento<UsuarioModel>()}/{usuarioDto.Id}")
              //.Child(ConfiguracionAnotacion.GetNombreDocumento<UsuarioModel>())
              .PatchAsync(JsonConvert.SerializeObject(usuarioDto));

            return obj;

        }

        public async Task<UsuarioModel> CrearUsuario(UsuarioModel usuario)
        {
            await _firebaseContext.GetFirebaseClient(usuario.Id)
             .Child($"{ConfiguracionAnotacion.GetNombreDocumento<UsuarioModel>()}/{usuario.Id}")
             .PostAsync(JsonConvert.SerializeObject(usuario), false);

            return usuario;

        }

#pragma warning disable S927 // parameter names should match base declaration and other partial definitions
        public async Task<UsuarioModel> ActualizarUsuario(UsuarioModel usuario, string contrasena, string tokenUser)
        {
            //J.Mamian - Se actualiza la contraseña del usuario.                            

#pragma warning disable CS4014 // parameter names should match base declaration and other partial definitions
            CambiarContrasena(contrasena);

            //J.Mamian - Se actualiza la información del usuario.
            await _usuarioDao.Actualizar(Helpers.UserId(_firebaseContext._httpContextAccessor.HttpContext.User), usuario, true);
            return usuario;
        }

        public async Task<FirebaseAuthLink> CambiarContrasena(string contrasena)
        {

            if (contrasena != "")
            {
                //return await _firebaseContext.Context.ChangeUserPassword()


                //J.Mamian - Se actualiza la contraseña del usuario.                            
                return await _firebaseContext.Context.ChangeUserPassword(Helpers.UserToken(_firebaseContext._httpContextAccessor.HttpContext.User), contrasena);
                //J.Mamian - Se actualiza la información del usuario.
                
                
            }
            return null;
        }



    }
}