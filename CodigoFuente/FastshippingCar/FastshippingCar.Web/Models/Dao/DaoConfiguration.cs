﻿using FastshippingCar.Web.Models.Dao;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.Extensions.DependencyInjection;

namespace FastshippingCar.Web.Services
{
    public static class DaoConfiguration
    {
        public static void DaoConfigurar(this IServiceCollection services)
        {
            services.AddScoped(typeof(IDaoGenerico<>), typeof(DaoGenericoImpl<>));
            services.AddScoped<IUsuarioDao, UsuarioDaoImpl>();
        }
    }
}
