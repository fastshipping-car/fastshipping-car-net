﻿using FastshippingCar.Web.Common;
using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Models.DbContext;
using Firebase.Database;
using Firebase.Database.Query;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Models.Dao
{
    public class DaoGenericoImpl<T> : IDaoGenerico<T> where T : ModelGenerico
    {
        private readonly FirebaseContext _firebaseContext;

        public DaoGenericoImpl(FirebaseContext firebaseContext)
        {
            _firebaseContext = firebaseContext;
        }

        public ChildQuery ChildQuery => _firebaseContext.GetFirebaseClient()
                                             .Child(ConfiguracionAnotacion.GetNombreDocumento<T>());

#pragma warning disable S927 // parameter names should match base declaration and other partial definitions
        public Task Actualizar(string id, T dato, bool onlyField = false)
#pragma warning restore S927 // parameter names should match base declaration and other partial definitions
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new System.ArgumentException("message", nameof(id));
            }

            if (dato is null)
            {
                throw new System.ArgumentNullException(nameof(dato));
            }

            if (onlyField)
            {
                return _firebaseContext.GetFirebaseClient()
                              .Child($"{ConfiguracionAnotacion.GetNombreDocumento<T>()}/{id}")
                              .PatchAsync(JsonConvert.SerializeObject(dato));
            }
            else
            {
                return _firebaseContext.GetFirebaseClient()
                                  .Child($"{ConfiguracionAnotacion.GetNombreDocumento<T>()}/{id}")
                                  .PutAsync(JsonConvert.SerializeObject(dato));
            }

        }

        public Task Borrar(string id)
        {
            return _firebaseContext.GetFirebaseClient()
              .Child($"{ConfiguracionAnotacion.GetNombreDocumento<T>()}/{id}")
              .DeleteAsync();
        }

        public virtual async Task<List<T>> Consultar(FilterQuery query = null)
        {
            IReadOnlyCollection<FirebaseObject<T>> dato = null;
            if (query is null)
            {
                dato = await _firebaseContext.GetFirebaseClient()
                                             .Child(ConfiguracionAnotacion.GetNombreDocumento<T>())
                                             .OnceAsync<T>();
            }
            else
            {
                dato = await query.OnceAsync<T>();
            }

            return dato.Select(x =>
            {
                x.Object.Id = x.Key;
                return x.Object;
            }).ToList();
        }

        public virtual async Task<T> Crear(T producto)
        {
            if (producto.Id is null)
            {
                var dinos = await _firebaseContext.GetFirebaseClient()
                  .Child(ConfiguracionAnotacion.GetNombreDocumento<T>())
                  .PostAsync(JsonConvert.SerializeObject(producto));
                producto.Id = dinos.Key;
            }
            else
            {
                await _firebaseContext.GetFirebaseClient()
                  .Child($"{ConfiguracionAnotacion.GetNombreDocumento<T>()}/{producto.Id}")
                  .PatchAsync(JsonConvert.SerializeObject(producto));
            }
            return producto;
        }

        public async Task<List<T>> Crear(List<T> producto)
        {
            var dinos = await _firebaseContext.GetFirebaseClient()
              .Child(ConfiguracionAnotacion.GetNombreDocumento<T>())
              .PostAsync(JsonConvert.SerializeObject(producto));
            return JsonConvert.DeserializeObject<List<T>>(dinos.Object);
        }

        public virtual async Task<T> Tomar(string id)
        {
            var dinos = await _firebaseContext.GetFirebaseClient()
              .Child($"{ConfiguracionAnotacion.GetNombreDocumento<T>()}/{id}")
              .OnceSingleAsync<T>();
            if (dinos != null)
            {
                dinos.Id = id;
            }
            return dinos;
        }
    }
}
