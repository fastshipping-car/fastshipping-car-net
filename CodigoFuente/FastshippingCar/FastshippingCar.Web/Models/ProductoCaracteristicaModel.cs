﻿using FastshippingCar.Web.Models.Enums;
using System.Collections.Generic;

namespace FastshippingCar.Web.Models
{
    public class ProductoCaracteristicaModel
    {
        public string Nombre { get; set; }
        public Dictionary<string, string> Valores { get; set; }
        public TipoConfiguracion TipoConfiguracion { get; set; }
    }
}
