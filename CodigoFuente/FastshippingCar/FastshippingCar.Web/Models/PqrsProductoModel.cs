﻿using FastshippingCar.Web.Common;

namespace FastshippingCar.Web.Models
{
    [NombreDocumento(Name = "pqrs-producto")]
    public class PqrsProductoModel : ModelGenerico
    {
        public string Titulo { get; set; }
        public string Mensaje { get; set; }
        public string ProductoId { get; set; }
        public string UsuarioId { get; set; }
        public string VendedorId { get; set; }
        public override string Id { get; set; }
    }
}
