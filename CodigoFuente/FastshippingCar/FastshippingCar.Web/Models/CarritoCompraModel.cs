﻿using FastshippingCar.Web.Common;
using Newtonsoft.Json;

namespace FastshippingCar.Web.Models
{
    [NombreDocumento(Name = "carrito-comprar")]
    public class CarritoCompraModel : ModelGenerico
    {
        public string UsuarioId { get; set; }
        public string ProductoId { get; set; }
        public string TiendaId { get; set; }
        public bool Pagado { get; set; }
        [JsonIgnore]
        public override string Id { get; set; }
    }
}
