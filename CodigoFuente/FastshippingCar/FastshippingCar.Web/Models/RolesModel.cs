﻿using FastshippingCar.Web.Common;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace FastshippingCar.Web.Models
{
    [NombreDocumento(Name = "roles")]
    public class RolesModel : ModelGenerico
    {
        public List<OpcionesModel> Opciones { get; set; }
        [JsonIgnore]
        public override string Id { get; set; } //No generar VE - ADM - CL
    }
}
