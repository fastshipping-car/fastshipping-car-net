﻿using FastshippingCar.Web.Common;
using Newtonsoft.Json;

namespace FastshippingCar.Web.Models
{
    [NombreDocumento(Name = "vendedores")]
    public class VendedorModel : ModelGenerico
    {
        [JsonIgnore]
        public override string Id { get; set; }
        public string TiendaId { get; set; }
        //public long NumeroFavoritos { get; set; }
        public string UsuarioId { get; set; }
    }
}
