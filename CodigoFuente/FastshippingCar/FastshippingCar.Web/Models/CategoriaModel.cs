﻿using FastshippingCar.Web.Common;
using System.ComponentModel.DataAnnotations;


namespace FastshippingCar.Web.Models
{
    [NombreDocumento(Name = "categorias")]
    public class CategoriaModel : ModelGenerico
    {
        [Required(ErrorMessage = "El {0} es requerido")]
        [DataType(DataType.Text)]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "El {0} es requerido")]
        [DataType(DataType.MultilineText)]
        public string Descripcion { get; set; }
        public override string Id { get; set; }
    }
}
