﻿using FastshippingCar.Web.Models;

namespace FastshippingCar.Web.Dto
{
    public class ProductoDto
    {
        public ProductoModel Producto { get; set; }
        public decimal ProductoPuntuacion { get; set; }
    }
}
