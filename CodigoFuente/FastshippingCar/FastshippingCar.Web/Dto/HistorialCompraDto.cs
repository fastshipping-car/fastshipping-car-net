﻿using FastshippingCar.Web.Models;

namespace FastshippingCar.Web.Dto
{
    public class HistorialCompraDto
    {
        public HistorialCompraModel HistorialCompra { get; set; }
        public decimal ProductoPuntuacion { get; set; }
    }
}
