﻿using FastshippingCar.Web.Models;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace FastshippingCar.Web.Dto
{
    public class UsuarioDto : UsuarioModel
    {
        [JsonIgnore]
        public string NombreCompleto => $"{Nombre} {Apellido}";

        [JsonIgnore]
        [Required(ErrorMessage = "La {0} es requerida")]
        [StringLength(255, ErrorMessage = "La {0} debe estar entre {2} a {1} caracteres", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Display(Name = "contraseña")]
        public string Contrasena { get; set; }

        [JsonIgnore]
        [Required(ErrorMessage = "El {0} es requerida")]
        [StringLength(255, ErrorMessage = "El campo {0} debe estar entre {2} a {1} caracteres", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Display(Name = "confirmar Contraseña")]
        [Compare(nameof(Contrasena), ErrorMessage = "El campo {1} y {0} deben ser iguales")]
        public string ConfirmarContrasena { get; set; }

        [JsonIgnore]
        [System.ComponentModel.Bindable(true)]
        [System.ComponentModel.SettingsBindable(true)]
        public bool IndicadorContrasena { get; set; }
                
    }
}
