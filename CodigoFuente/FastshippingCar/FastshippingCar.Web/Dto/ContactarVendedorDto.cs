﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Dto
{
    public class ContactarVendedorDto
    {
        [Required]
        [DataType(DataType.Text)]
        public string VendedorId { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        public string Mensaje { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Asunto { get; set; }
    }
}
