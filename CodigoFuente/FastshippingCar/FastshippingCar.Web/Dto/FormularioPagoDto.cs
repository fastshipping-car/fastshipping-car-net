﻿using FastshippingCar.Web.Settings;
using System;
using System.Security.Cryptography;
using System.Text;

namespace FastshippingCar.Web.Dto
{
    public class FormularioPagoDto
    {
        public int MerchantId { get; private set; }
        public int AccountId { get; private set; }
        public string Description { get; set; }
        public string ReferenceCode { get; set; }
        public long Amount { get; set; }
        public long SendCost { get; set; }
        public int Tax { get; set; }
        public int TaxReturnBase { get; set; }
        public string Currency { get; private set; }
        public string Signature { get; private set; }
        public void CalcularSignature(PayUConfiguration payUConfiguration)
        {
            MerchantId = payUConfiguration.MerchantId;
            AccountId = payUConfiguration.AccountId;
            Currency = payUConfiguration.Currency;
            Test = Convert.ToByte(payUConfiguration.Test);

            using MD5 mySHA256 = MD5.Create();
            // Compute the hash of the fileStream.
            byte[] hashValue = mySHA256.ComputeHash(Encoding.ASCII.GetBytes($"{payUConfiguration.ApiKey}~{MerchantId}~{ReferenceCode}~{Amount}~{Currency}"));
            //return BitConverter.ToString(hashValue);
            // Convert byte array to a string   
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < hashValue.Length; i++)
            {
                builder.Append(hashValue[i].ToString("x2"));
            }
            Signature = builder.ToString();
        }
        public int Test { get; private set; }
    }
}
