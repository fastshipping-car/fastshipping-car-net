﻿using FastshippingCar.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastshippingCar.Web.Dto
{
    public class VendedorDto
    {
        public VendedorModel Vendedor { get; set; }
        public UsuarioModel Usuario { get; set; }
    }
}
