﻿using System;

namespace FastshippingCar.Web.Common
{
    public class NombreDocumentoAttribute : Attribute
    {
        public NombreDocumentoAttribute()
        {
        }

        public NombreDocumentoAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
