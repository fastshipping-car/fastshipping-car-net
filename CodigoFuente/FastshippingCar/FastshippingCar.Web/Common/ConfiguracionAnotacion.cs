﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace FastshippingCar.Web.Common
{
    public static class ConfiguracionAnotacion
    {
        public static string GetNombreDocumento<T>() where T : class
        {
            var atributo = typeof(T).GetCustomAttributes(typeof(NombreDocumentoAttribute), true).Cast<NombreDocumentoAttribute>().FirstOrDefault();
            if (atributo is null)
            {
                throw new ValidationException($"{nameof(NombreDocumentoAttribute)} no esta configurado en {nameof(T)}");
            }

            return atributo.Name;
        }
    }
}
