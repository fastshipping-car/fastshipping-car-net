﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace FastshippingCar.Web.Common
{
    public class Helpers
    {
        public static bool IsAutentication(ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Identity.IsAuthenticated;
        }

        public static string UserName(ClaimsPrincipal claimsPrincipal)
        {
            return GetClaims(claimsPrincipal, ClaimTypes.NameIdentifier).FirstOrDefault().Value;
        }

        public static string UserToken(ClaimsPrincipal claimsPrincipal)
        {
            return GetClaims(claimsPrincipal, ClaimTypes.Sid).FirstOrDefault().Value;
        }

        public static string UserEmail(ClaimsPrincipal claimsPrincipal)
        {
            return GetClaims(claimsPrincipal, ClaimTypes.Email).FirstOrDefault().Value;
        }
        public static string UserId(ClaimsPrincipal claimsPrincipal)
        {
            return GetClaims(claimsPrincipal, ClaimTypes.PrimarySid).FirstOrDefault().Value;
        }
        public static string RolId(ClaimsPrincipal claimsPrincipal)
        {
            if (claimsPrincipal.Identity.IsAuthenticated)
            {
                return GetClaims(claimsPrincipal, ClaimTypes.Role).FirstOrDefault().Value;
            }
            return null;
        }
        public static List<Claim> GetClaims(ClaimsPrincipal claimsPrincipal, string claimTypes)
        {
            return claimsPrincipal.Claims.Where(x => x.Type.Equals(claimTypes)).ToList();
        }
    }
}
