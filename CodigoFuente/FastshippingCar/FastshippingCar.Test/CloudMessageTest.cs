﻿using FastshippingCar.Test.Helper;
using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Models.DbContext;
using FirebaseAdmin.Messaging;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FastshippingCar.Test
{
    class CloudMessageTest
    {
        private ServiceProvider serviceProvider;

        [SetUp]
        public void Setup()
        {
            serviceProvider = CargarDependencia.Setup();
        }

        [Test]
        public async Task EnviarMensaje()
        {
            using var service = serviceProvider.CreateScope();
            var producto = service.ServiceProvider.GetRequiredService<FirebaseContext>();
            await producto.SendMail("", "", "");
            Assert.Pass();
        }
    }
}
