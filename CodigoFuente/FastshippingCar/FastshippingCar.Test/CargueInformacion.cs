﻿using Bogus;
using FastshippingCar.Test.Helper;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using FastshippingCar.Web.Models.Enums;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FastshippingCar.Test
{
    public class CargueInformacion
    {
        private ServiceProvider serviceProvider;
        private IServiceScope service;

        [SetUp]
        public void Setup()
        {
            serviceProvider = CargarDependencia.Setup();
            service = serviceProvider.CreateScope();
        }

        public async Task Autenticacion()
        {
            var login = service.ServiceProvider.GetRequiredService<IUsuarioDao>();
            var httpContext = service.ServiceProvider.GetRequiredService<IHttpContextAccessor>();
            var usuario = service.ServiceProvider.GetRequiredService<IDaoGenerico<UsuarioModel>>();
            var firebaseAuthLink = await login.AutenticarUsuario("roldan9612@gmail.com", "123456rr");
            var infoUsuario = await usuario.Tomar(firebaseAuthLink.User.LocalId);
            var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.NameIdentifier, firebaseAuthLink.User.DisplayName),
                        new Claim(ClaimTypes.Name, firebaseAuthLink.User.FirstName),
                        new Claim(ClaimTypes.Email, firebaseAuthLink.User.Email),
                        new Claim(ClaimTypes.Sid, firebaseAuthLink.FirebaseToken),
                        new Claim(ClaimTypes.Role, infoUsuario.RolId),
                        new Claim(ClaimTypes.IsPersistent, !firebaseAuthLink.IsExpired() ? bool.TrueString : bool.FalseString),
                    };
            httpContext.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.NameIdentifier, ClaimTypes.Role));
        }

        [Test]
#pragma warning disable S2699 // Tests should include assertions
        public async Task CargaProductos()
#pragma warning restore S2699 // Tests should include assertions
        {
            await Autenticacion();
            var producto = service.ServiceProvider.GetRequiredService<IDaoGenerico<ProductoModel>>();
            var testDetalle = new Faker<ProductoDetalleModel>().RuleFor(o => o.Titulo, f => f.Lorem.Paragraph()).RuleFor(o => o.Descripcion, f => f.Lorem.Lines(2)).Generate(3).ToList();
            var testOrders = new Faker<ProductoModel>()
                                .RuleFor(o => o.Nombre, f => f.Commerce.ProductMaterial())
                                //.RuleFor(o => o.VendedorId, f => "-M7pXvHj1lJ4JXAZN5xF")
                                .RuleFor(o => o.Porcentaje, f => f.Random.Short(0, 80))
                                .RuleFor(o => o.Precio, f => f.Random.Long(90000, 900000))
                                .RuleFor(o => o.ProductoDetalles, f => testDetalle)
                                .RuleFor(o => o.Imagenes, f => Enumerable.Range(1, 5)
                                      .Select(_ => new Faker().Image.PicsumUrl(1928, 1080))
                                      .ToList()).Generate(50).ToList();
            foreach (var item in testOrders)
            {
                await producto.Crear(item);
            }
        }

        [Test]
#pragma warning disable S2699 // Tests should include assertions
        public async Task CargaVendedor()
#pragma warning restore S2699 // Tests should include assertions
        {
            await Autenticacion();
            var producto = service.ServiceProvider.GetRequiredService<IDaoGenerico<VendedorModel>>();
            var item = new VendedorModel
            {
                UsuarioId = "mHDxEaNCbLOg3Wq8kBXyqBnMwKp2",
                TiendaId = "Fastshipping Car"
            };
            await producto.Crear(item);

        }

        [Test]
#pragma warning disable S2699 // Tests should include assertions
        public async Task CargaCategoria()
#pragma warning restore S2699 // Tests should include assertions
        {
            await Autenticacion();
            var categoria = service.ServiceProvider.GetRequiredService<IDaoGenerico<CategoriaModel>>();
            var categorias = new List<CategoriaModel>
            {
                new CategoriaModel
                {
                    Nombre = "Tecnologia",
                    Descripcion = "Cambios a traves de lo digital",
                },
                new CategoriaModel
                {
                    Nombre = "Vehiculos",
                    Descripcion = "Encuentra todo a cerca que puedes comprar para tu vehiculo",
                },
                new CategoriaModel
                {
                    Nombre = "Hogar",
                    Descripcion = "Encuentra todo a cerca que puedes comprar para tu hogar",
                },
            };
            foreach (var item in categorias)
            {
                await categoria.Crear(item);
            }
        }

        [Test]
#pragma warning disable S2699 // Tests should include assertions
        public async Task CargaRol()
#pragma warning restore S2699 // Tests should include assertions
        {
            await Autenticacion();
            var rolService = service.ServiceProvider.GetRequiredService<IDaoGenerico<RolesModel>>();
            var roles = new List<RolesModel>
            {
                new RolesModel
                {
                    Id = Roles.CLI.ToString(),
                    Opciones = new List<OpcionesModel>
                    {
                        new OpcionesModel
                        {
                            Descripcion = "Categorias",
                            Url = "#",
                            Opciones = new List<OpcionesModel>
                            {
                                new OpcionesModel
                                {
                                    Descripcion ="Crear Categorias",
                                    Url = "/Categorias/crearCategoria"
                                },
                                new OpcionesModel
                                {
                                    Descripcion ="Editar Categorias",
                                    Url = "/Categorias/editarCategoria"
                                }
                            }
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Mis Productos",
                            Url = "/misProductos",
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Ventas",
                            Url = "#",
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Medios de Pago",
                            Url = "/informacionPago",
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Preguntas Frecuentes",
                            Url = "#",
                        }
                    }
                },
                new RolesModel
                {
                    Id = Roles.ADM.ToString(),
                    Opciones = new List<OpcionesModel>
                    {
                        new OpcionesModel
                        {
                            Descripcion = "Categorias",
                            Url = "#",
                            Opciones = new List<OpcionesModel>
                            {
                                new OpcionesModel
                                {
                                    Descripcion ="Crear Categorias",
                                    Url = "/Categorias/crearCategoria"
                                },
                                new OpcionesModel
                                {
                                    Descripcion ="Editar Categorias",
                                    Url = "/Categorias/editarCategoria"
                                }
                            }
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Mis Productos",
                            Url = "/misProductos",
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Ventas",
                            Url = "#",
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Medios de Pago",
                            Url = "/informacionPago",
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Preguntas Frecuentes",
                            Url = "#",
                        }
                    }
                },
                new RolesModel
                {
                    Id = Roles.VEN.ToString(),
                    Opciones = new List<OpcionesModel>
                    {
                        new OpcionesModel
                        {
                            Descripcion = "Categorias",
                            Url = "#",
                            Opciones = new List<OpcionesModel>
                            {
                                new OpcionesModel
                                {
                                    Descripcion ="Crear Categorias",
                                    Url = "/Categorias/crearCategoria"
                                },
                                new OpcionesModel
                                {
                                    Descripcion ="Editar Categorias",
                                    Url = "/Categorias/editarCategoria"
                                }
                            }
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Mis Productos",
                            Url = "/misProductos",
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Ventas",
                            Url = "#",
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Medios de Pago",
                            Url = "/informacionPago",
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Preguntas Frecuentes",
                            Url = "#",
                        },
                        new OpcionesModel
                        {
                            Descripcion = "Tiendas",
                            Url = "#",
                            Opciones = new List<OpcionesModel>
                            {
                                new OpcionesModel
                                {
                                    Descripcion ="Crear Tiendas",
                                    Url = "/Tiendas/crearTienda"
                                },
                                new OpcionesModel
                                {
                                    Descripcion ="Editar Tiendas",
                                    Url = "/Tiendas/editarTienda"
                                }
                            }
                        }

                    }
                }
            };
            foreach (var item in roles)
            {
                await rolService.Crear(item);
            }
        }
    }
}
