using FastshippingCar.Test.Helper;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Firebase.Auth;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Threading.Tasks;

namespace FastshippingCar.Test
{
    public class Tests
    {
        private ServiceProvider serviceProvider;

        [SetUp]
        public void Setup()
        {
            serviceProvider = CargarDependencia.Setup();
        }

        [Test]
        public async Task Autenticacion()
        {
            using var service = serviceProvider.CreateScope();
            var login = service.ServiceProvider.GetRequiredService<IUsuarioDao>();

            var dato = await login.AutenticarUsuario("roldan9612@gmail.com", "123456rr");
            Assert.IsNotNull(dato);
            Assert.IsNotNull(dato.FirebaseToken);
            Assert.IsNotEmpty(dato.FirebaseToken);
            Assert.AreEqual("roldan9612@gmail.com", dato.User.Email);
        }

        [Test]
        public async Task MalAutenticacion()
        {
            using var service = serviceProvider.CreateScope();
            var login = service.ServiceProvider.GetRequiredService<IUsuarioDao>();
            try
            {
                await login.AutenticarUsuario("iuytdcgklasd@gmail.com", "123456rr");
                Assert.Fail("El email no debe existir");
            }
            catch (FirebaseAuthException e)
            {
                Assert.IsNotNull(e);
                Assert.AreEqual(e.Reason, AuthErrorReason.UnknownEmailAddress);
            }

        }
    }
}