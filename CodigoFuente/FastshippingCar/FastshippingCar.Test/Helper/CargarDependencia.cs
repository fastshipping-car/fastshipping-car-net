﻿using FastshippingCar.Web.Models.DbContext;
using FastshippingCar.Web.Services;
using FastshippingCar.Web.Settings;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NUnit.Framework;

namespace FastshippingCar.Test.Helper
{
    public static class CargarDependencia
    {
        private static ServiceProvider serviceProvider;

        public static ServiceProvider Setup()
        {
            if (!(serviceProvider is null))
                return serviceProvider;

            var configuration = GetIConfigurationRoot(TestContext.CurrentContext.TestDirectory);
            var services = new ServiceCollection();
            services.AddLogging(config =>
            {
                config.AddDebug();
                config.AddConsole();
            });

            services.AddSingleton<IConfiguration>(provider => configuration);
            services.CargarConfiguracion(configuration);
            services.AddSingleton<IHttpContextAccessor, TestHttpContextAccessor>();
            //services.AddHttpContextAccessor();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(c =>
            {
                c.Cookie.Name = "FastshippingCar.Web";
                c.LogoutPath = "/";
                c.LoginPath = "/Login";
            });
            services.AddDataProtection();
            services.AddSingleton<FirebaseContext>();
            services.DaoConfigurar();
            services.ServiceConfigurar();
            services.AddRazorPages();
            serviceProvider = services.BuildServiceProvider();
            return serviceProvider;
        }

        public static IConfigurationRoot GetIConfigurationRoot(string outputPath)
        {
            return new ConfigurationBuilder()
                .SetBasePath(outputPath)
                .AddJsonFile("appsettings.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
        }

    }
}
