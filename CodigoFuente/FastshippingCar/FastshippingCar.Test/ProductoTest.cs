﻿using FastshippingCar.Test.Helper;
using FastshippingCar.Web.Models;
using FastshippingCar.Web.Models.Dao.Intefaces;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FastshippingCar.Test
{
    class ProductoTest
    {
        private ServiceProvider serviceProvider;

        [SetUp]
        public void Setup()
        {
            serviceProvider = CargarDependencia.Setup();
        }

        [Test]
        public async Task ConsultarProductos()
        {
            using var service = serviceProvider.CreateScope();
            var login = service.ServiceProvider.GetRequiredService<IUsuarioDao>();
            var httpContext = service.ServiceProvider.GetRequiredService<IHttpContextAccessor>();
            var firebaseAuthLink = await login.AutenticarUsuario("roldan9612@gmail.com", "123456rr");
            var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.NameIdentifier, firebaseAuthLink.User.DisplayName),
                        new Claim(ClaimTypes.Name, firebaseAuthLink.User.FirstName),
                        new Claim(ClaimTypes.Email, firebaseAuthLink.User.Email),
                        new Claim(ClaimTypes.Sid, firebaseAuthLink.FirebaseToken),
                        new Claim(ClaimTypes.IsPersistent, !firebaseAuthLink.IsExpired() ? bool.TrueString : bool.FalseString),
                    };
            httpContext.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.NameIdentifier, ClaimTypes.Role));

            var producto = service.ServiceProvider.GetRequiredService<IDaoGenerico<ProductoModel>>();

            var dato = await producto.Consultar();
            Assert.IsNotNull(dato);
            foreach (var item in dato)
            {
                Assert.IsNotNull(item.Id);
            }
        }

        [Test]
        public async Task TomarProducto()
        {
            using var service = serviceProvider.CreateScope();
            var login = service.ServiceProvider.GetRequiredService<IUsuarioDao>();
            var httpContext = service.ServiceProvider.GetRequiredService<IHttpContextAccessor>();
            var firebaseAuthLink = await login.AutenticarUsuario("roldan9612@gmail.com", "123456rr");
            var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.NameIdentifier, firebaseAuthLink.User.DisplayName),
                        new Claim(ClaimTypes.Name, firebaseAuthLink.User.FirstName),
                        new Claim(ClaimTypes.Email, firebaseAuthLink.User.Email),
                        new Claim(ClaimTypes.Sid, firebaseAuthLink.FirebaseToken),
                        new Claim(ClaimTypes.IsPersistent, !firebaseAuthLink.IsExpired() ? bool.TrueString : bool.FalseString),
                    };
            httpContext.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.NameIdentifier, ClaimTypes.Role));

            var producto = service.ServiceProvider.GetRequiredService<IDaoGenerico<ProductoModel>>();

            var dato = await producto.Tomar("1");
            Assert.IsNotNull(dato);
            Assert.IsNotNull(dato.Id);
        }

        [Test]
        public async Task CrearProducto()
        {
            using var service = serviceProvider.CreateScope();
            var login = service.ServiceProvider.GetRequiredService<IUsuarioDao>();
            var httpContext = service.ServiceProvider.GetRequiredService<IHttpContextAccessor>();
            var firebaseAuthLink = await login.AutenticarUsuario("roldan9612@gmail.com", "123456rr");
            var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.NameIdentifier, firebaseAuthLink.User.DisplayName),
                        new Claim(ClaimTypes.Name, firebaseAuthLink.User.FirstName),
                        new Claim(ClaimTypes.Email, firebaseAuthLink.User.Email),
                        new Claim(ClaimTypes.Sid, firebaseAuthLink.FirebaseToken),
                        new Claim(ClaimTypes.IsPersistent, !firebaseAuthLink.IsExpired() ? bool.TrueString : bool.FalseString),
                    };
            httpContext.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.NameIdentifier, ClaimTypes.Role));

            var producto = service.ServiceProvider.GetRequiredService<IDaoGenerico<ProductoModel>>();
            var test = new ProductoModel
            {
                Nombre = "productotest",
                Precio = 5000,
                Porcentaje = 25
            };
            var dato = await producto.Crear(test);
            Assert.IsNotNull(dato);
            Assert.IsNotNull(dato.Id);
        }
    }
}